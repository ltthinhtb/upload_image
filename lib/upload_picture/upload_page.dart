import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:upload_picture/generated/l10n.dart';
import 'package:upload_picture/upload_picture/upload_bloc.dart';
import 'package:upload_picture/upload_picture/upload_event.dart';
import 'package:upload_picture/upload_picture/upload_state.dart';

class PictureUploadPage extends StatefulWidget {
  @override
  _PictureUploadPageState createState() => _PictureUploadPageState();
}

class _PictureUploadPageState extends State<PictureUploadPage> {
  // ignore: close_sinks
  UploadBloc _bloc;
  List<Asset> images = List<Asset>();

  Widget buildListView() {
    if (images != null)
      return ListView(
        children: List.generate(images.length, (index) {
          Asset asset = images[index];
          return AssetThumb(
            asset: asset,
            width: 300,
            height: 300,
          );
        }),
      );
    else
      return Container(color: Colors.white);
  }

  @override
  void initState() {
    super.initState();
    _bloc = UploadBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _bloc,
      child: BlocBuilder<UploadBloc, UploadState>(
          builder: (BuildContext context, UploadState state) {
        return Scaffold(
          appBar: AppBar(title: Text(S.of(context).name)),
          body: Container(
            child: Center(
              child: ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 20, top: 30),
                    child: Text(
                      S.of(context).add,
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                  Container(
                    height: 200.0,
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Card(
                        child: _bloc.images.length == 0
                            ? Center(
                                child: GestureDetector(
                                  child: Text(S.of(context).click),
                                  onTap: () {
                                    BlocProvider.of<UploadBloc>(context)
                                        .add(LoadAssets());
                                  },
                                ),
                              )
                            : ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: _bloc.images.length,
                                itemBuilder: (BuildContext context, int index) {
                                  Asset asset = _bloc.images[index];
                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: AssetThumb(
                                        asset: asset, width: 160, height: 160),
                                  );
                                }),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      S.of(context).write,
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Card(
                        child: Container(
                            width: 100.0,
                            child: new TextField(
                                style: new TextStyle(
                                    fontSize: 40.0,
                                    height: 2.0,
                                    color: Colors.black)))),
                  )
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
