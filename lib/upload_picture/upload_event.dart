import 'package:equatable/equatable.dart';

abstract class UploadEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadAssets extends UploadEvent{}

class AppStated extends UploadEvent{}