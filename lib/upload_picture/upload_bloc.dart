
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:upload_picture/upload_picture/upload_event.dart';
import 'package:upload_picture/upload_picture/upload_state.dart';

class UploadBloc extends Bloc<UploadEvent,UploadState>{
  List<Asset> images = List<Asset>();
  @override
  UploadState get initialState => Initial();
  @override
  Stream<UploadState> mapEventToState(UploadEvent event) async* {
    if(event is LoadAssets) {
      try {
        images = await MultiImagePicker.pickImages(
          maxImages: 300,
          enableCamera: true,
          selectedAssets: images,
          cupertinoOptions: CupertinoOptions(takePhotoIcon: "Chụp Ảnh"),
          materialOptions: MaterialOptions(
            actionBarColor: "#abcdef",
            actionBarTitle: "Thư Viện",
            allViewTitle: "Tất cả Ảnh",
            useDetailsView: false,
            selectCircleStrokeColor: "#000000",
          ),
        );
      } on Exception catch (e) {
        e.toString();
      }
      yield UploadSuccessful(images);
    }
  }
}