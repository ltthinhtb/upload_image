import 'package:equatable/equatable.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

abstract class UploadState extends Equatable{
  const UploadState();
  @override
  List<Object> get props => [];
}

class Initial extends UploadState{
  @override
  String toString() => 'ListPaymentLoading';
}

class UploadUnSuccessful extends UploadState{
  @override
  String toString() => 'UploadUnSuccessful';
}

class UploadSuccessful extends UploadState{
  final List<Asset> resultList;
  UploadSuccessful(this.resultList);
  @override
  String toString() => 'UploadSuccessful';
}