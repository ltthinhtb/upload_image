import 'package:flutter/material.dart';
import 'package:upload_picture/generated/l10n.dart';
import 'package:upload_picture/upload_picture/upload_page.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: [S.delegate],
        supportedLocales: S.delegate.supportedLocales,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: PictureUploadPage()
    );
  }
}
