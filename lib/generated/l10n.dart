// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

class S {
  S(this.localeName);
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return S(localeName);
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  final String localeName;

  String get click {
    return Intl.message(
      'Tải lên',
      name: 'click',
      desc: '',
      args: [],
    );
  }

  String get add {
    return Intl.message(
      'Tải ảnh hoặc video',
      name: 'add',
      desc: '',
      args: [],
    );
  }

  String get error {
    return Intl.message(
      'Không có Ảnh Nào',
      name: 'error',
      desc: '',
      args: [],
    );
  }

  String get allPhoto {
    return Intl.message(
      'Tất cả Ảnh',
      name: 'allPhoto',
      desc: '',
      args: [],
    );
  }

  String get name {
    return Intl.message(
      'Trang cá nhân',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  String get write {
    return Intl.message(
      'Viết nhận xét của bạn',
      name: 'write',
      desc: '',
      args: [],
    );
  }

  String get hideText {
    return Intl.message(
      'Sản phẩm có làm bạn hài lòng ?',
      name: 'hideText',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale('en', ''),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (Locale supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}